# Tucan

A quirky font that's all character, colour and charm.

Made in the [Type:Bits workshop](https://typebits.gitlab.io) in Tomelloso, 2016.

![Font preview](https://gitlab.com/typebits/font-tucan/-/jobs/artifacts/master/raw/Tucan-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-tucan/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-tucan/-/jobs/artifacts/master/raw/Tucan-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-tucan/-/jobs/artifacts/master/raw/Tucan-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-tucan/-/jobs/artifacts/master/raw/Tucan-Regular.sfd?job=build-font)

# Authors

* Maria A. Bautista
* Carolina O. Canton
* Javier C. Carpintero
* Alba M. Gomez
* Azucena S. Navarro

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
